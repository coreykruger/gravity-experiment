#!/usr/bin/env node

var http = require('http');
var fs = require('fs');

var server = http.createServer(function(request, response) {

    var url = '.' + (request.url == '/' ? '/index.html' : request.url);
  
    fs.readFile(url, function (err, data) {
        if (err) {
            console.log(err);
            response.writeHead(500);
            return response.end('Error loading index.html');
        }

        var tmp     = url.lastIndexOf(".");
        var ext     = url.substring((tmp + 1));
        var mime    = mimes[ext] || 'text/plain';

        response.writeHead(200, { 'Content-Type': mime });
        response.end(data, 'utf-8');

    });
    console.log((new Date()) + ' Received request for ' + request.url);
});

server.listen(8080, '127.0.0.1', function() {
	console.log((new Date()) + ' Server is listening on port 8080');
});

var mimes = {
    'css':  'text/css',
    'js':   'text/javascript',
    'htm':  'text/html',
    'html': 'text/html',
    'ico':  'image/vnd.microsoft.icon'
};