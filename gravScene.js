

var screen = document.getElementById('screen');
screen.style.width = 600 + "px";
screen.style.height = 500 + "px";
var scene = new THREE.Scene();

var camera = new THREE.PerspectiveCamera();
camera.position.z = 200;
scene.add( camera );

renderer = new THREE.WebGLRenderer();

renderer.setSize( 600, 500 );
screen.appendChild( renderer.domElement );
renderer.setClearColor ( new THREE.Color(0x444444), 1 );
//  END RENDERER

var controls = new THREE.OrbitControls( camera, renderer.domElement );

var light = new THREE.PointLight( 0xff0000, 1, 400 );
light.position.set( 50, 50, 50 );
scene.add( light );

//  cube
var cube = new THREE.Mesh( new THREE.CubeGeometry( 50, 50, 50 ), new THREE.MeshLambertMaterial( { color: 0xFF0000 } ) );
cube.name = "cube";
cube.rotation.set( Math.PI / 4, Math.PI / 4, 0 );
cube.position.set( 2, 0, 0 );
scene.add(cube);

camera.updateProjectionMatrix();

function render() {
	
	requestAnimationFrame(render);
	renderer.render(scene, camera);
	
}

render();